import os
from bs4 import BeautifulSoup
import string as st
import urllib
import urllib.request as urlreq
import xlwt
import numpy as np
import xlrd, datetime
import pytz
from isoweek import Week
from icalendar import Calendar, Event
from selenium import webdriver
from selenium.webdriver.support.ui import Select

def populate_dep():
    print(('=' * 30) + ' Running population script ' +('=' * 30))
    for dep in Department.objects.all():
            print(('='*30 + ' ') + dep.name + (' '+'='*30))
            driver = webdriver.PhantomJS()
            driver.get('http://keaplan.kea.dk/sws/prodF2018/default.aspx')

            main_window = None
            popup_window = None
            while main_window is None:
                    main_window = driver.current_window_handle

            driver.find_element_by_id('LinkBtn_studentset').click()
            filter_select = Select(driver.find_element_by_id('dlFilter'))  # select BYG
            filter_select.select_by_visible_text(dep.name.upper())
            obj_select = Select(driver.find_element_by_id('dlObject'))  # select BAKINT-2I
            options = obj_select.options
            for opt in options:
                name = opt.text
                try:
                    hold = Hold.objects.get(name=name)
                    print("Object already exists")
                except:
                    print("Object does not exist")
                    hold = Hold(name=name, department=dep)
                    hold.save()
def populate_courses():
    for hold in Hold.objects.all():
        print(hold.department, hold.name)

if __name__ == '__main__':
    import django
    os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                          'keapp.settings')
    django.setup()
    from main.models import Department, Hold
    #populate_dep()  # Call the populate function, which calls the
                # add_genre and add_musician functions
    populate_courses()
