from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView
from django.views import generic
from selenium import webdriver
from main.models import Department
from selenium.webdriver.support.ui import Select


# Create your views here.
class IndexView(generic.ListView):
    template_name = 'index.html'
    context_object_name = 'departments_list'

    def get_queryset(self):
        return Department.objects.all()

class DepartmentView(generic.DetailView):
    model = Department
    template_name="depdetail.html"
    slug_field = "name"
    slug_url_kwarg = "name"



def submit(request):
    team = request.POST.get('Team')
    team = team + ', F18'
    selector(team)
    context = {'team': team}
    return render(request, 'submit.html', context)

def selector(team):
    department = 'BYG'
    #team = 'BAKINT-2I, F18'
    driver = webdriver.PhantomJS()
    driver.get('http://keaplan.kea.dk/sws/prodF2018/default.aspx')

    main_window = None
    popup_window = None
    while main_window is None:
        main_window = driver.current_window_handle

    driver.find_element_by_id('LinkBtn_studentset').click()
    filter_select = Select(driver.find_element_by_id('dlFilter'))  # select BYG
    filter_select.select_by_visible_text(department)
    obj_select = Select(driver.find_element_by_id('dlObject'))  # select BAKINT-2I
    obj_select.select_by_visible_text(team)
    week_select = Select(driver.find_element_by_id('lbWeeks'))
    week_select.select_by_visible_text('Week 05')

    driver.find_element_by_id('bGetTimetable').click()

    while popup_window is None:  # get handle to popup
        for handle in driver.window_handles:
            if handle == main_window:
                continue
            popup_window = handle
            break

    driver.switch_to.window(popup_window)
    file_ = open('keapptest.html', 'w')
    file_.write(driver.page_source)
    file_.close()
