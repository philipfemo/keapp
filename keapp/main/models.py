from django.db import models

# Create your models here.
class Department(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    def __str__(self):
        return self.name

class Hold(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class CourseEvent(models.Model):
    teacher = models.CharField(max_length=50, blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    week = models.IntegerField(blank=True, null=True)
    summary = models.CharField(max_length=200, blank=True, null=True)
