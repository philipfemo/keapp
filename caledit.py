from icalendar import Calendar, Event
import shutil

"""
Thought process:
1) We have a calendar with weeks. m_week = master_week[week#]
2) We have THIS weeks calendar: cur_week

We wish to:
    Compare changes on THIS cur_week with m_week:
        1) If no changes - do nothing
        2) If changes - replace m_week with cur_week
        3) Output new master_week.ics

How to do it:
    * parse m_week into array
    * parse cur_week int array
    * if changes, delete events in m_week
    * create events from cur_week in m_week
"""
g = open('caltest.ics','rb')
m = open('foo.ics', 'wb')
startcal = Calendar.from_ical(g.read())
for component in startcal.walk():
    print(component, '\n')

# for component in gcal.walk():
#     if component.name == "VEVENT":
#         event = []
#         print('-----------------------')
#         print(component.get('summary'))
#         print(component.get('dtstart').dt)
#         print(component.get('dtend').dt)
#         print(component.get('dtstamp').dt)
#         print('-----------------------')
#         event.append(str(component.get('summary')))
#         event.append(str(component.get('dtstart').dt))
#         event.append(str(component.get('dtend').dt))
#         event.append(str(component.get('dtstamp').dt))
#         events.append(event)
g.close()
