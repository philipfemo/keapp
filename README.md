The purpose of Keapp is to create an ics feed for calendar apps that students
can subcribe to in order to see their latest schedules.

This entails:


    BACKEND:
        Scraping "keaplan.kea.dk":
            Scraping the following for each programme:
                - Master schedule for each "hold".
                - This weeks schedule for each "hold"
        Creating a master calendar for each programme and team based on:
            - The KEA master plan
            - Compared w. changes for this week.
            - Outputs: Updated master plan
    
    Consider:
        Use Django models to create a database that maintains the system
            - Could be necessary for URL Routing for the subscription feed
    
    FRONTEND:
        - Create an interface for choosing programme and "hold"
        - Provide subscription link based on input
        
        FUTURE Considerations:
        - Display this weeks schedule in the frontend
        