from bs4 import BeautifulSoup
import string as st
import urllib
import urllib.request as urlreq
import xlwt
import numpy as np
import xlrd, datetime
import pytz
from isoweek import Week
from icalendar import Calendar, Event
from selenium import webdriver
from selenium.webdriver.support.ui import Select

def get_date(weekNo, day):
    if day == "mandag":
        return Week(2018, weekNo).monday()
    elif day == "tirsdag":
        return Week(2018, weekNo).tuesday()
    elif day == "onsdag":
        return Week(2018, weekNo).wednesday()
    elif day == "torsdag":
        return Week(2018, weekNo).thursday()
    elif day == "fredag":
        return Week(2018, weekNo).friday()

def get_courses(tabel):
    rows = tabel.find("tbody").find_all("tr", recursive=False)
    for tr in rows[1:]:
        cols = tr.find_all("td")
        week = int(cols[5].text)
        dag = day
        print(30*'-')
        print(day)
        print("Course :" + cols[2].text + "\n")
        print("Start Time :" + cols[0].text + "\n")
        print("End Time :" + cols[1].text + "\n")
        print("Teacher :" + cols[3].text + "\n")
        print("Location :" + cols[4].text + "\n")
        print("Week :" + cols[5].text + "\n")
        print("Date :", get_date(week, dag), "\n")
        print(30*'-')

teams = [{'name': 'BAKINT-2I, EFTERÅR18', 'department': 'BYG'}]
dep = ['BYG', 'DES', 'DIG', 'TEK']
for x in range(1,2):
    print(('='*30 + ' ') + 'BAKINT-2I, EFTERÅR18' + (' '+'='*30))
    driver = webdriver.PhantomJS()
    driver.get('http://keaplan.kea.dk/sws/prodF2018/default.aspx')

    main_window = None
    popup_window = None
    while main_window is None:
            main_window = driver.current_window_handle

    driver.find_element_by_id('LinkBtn_studentset').click()
    filter_select = Select(driver.find_element_by_id('dlFilter'))  # select BYG
    filter_select.select_by_visible_text('BYG')
    obj_select = Select(driver.find_element_by_id('dlObject'))  # select BAKINT-2I
    obj_select.select_by_visible_text('BAKINT-2I, F18')
    week_select = Select(driver.find_element_by_id('lbWeeks'))
    week_select.select_by_visible_text('F2018')
    skemavisning = Select(driver.find_element_by_id('dlType'))
    skemavisning.select_by_visible_text('Week Timetable')
    driver.find_element_by_id('bGetTimetable').click()

    while popup_window is None:  # get handle to popup
        for handle in driver.window_handles:
            if handle == main_window:
                continue
            popup_window = handle
            break

    driver.switch_to.window(popup_window)
    file_ = open('allweeks.html', 'w')
    file_.write(driver.page_source)
    file_.close()



# soup = BeautifulSoup(open("allweeks.html"), 'lxml')
# tables = soup.findAll("table",{"class":"spreadsheet"})
# count = 0
# weeks = soup.findAll("span",{"class":"labelone"})
# days = soup.findAll("span",{"class":"labeltwo"})
# weeks_arr = []
# for week in weeks:
#     weeks_arr.append(int(week.text))
# for table in tables:
#     #############################################
#     ## insert the code below (outside the loop)##
#     #############################################
#     day = days[count].text
#     count = count + 1
#     get_courses(table)



# tabel = tables[0]
# day = days[0].text
# rows = tabel.find("tbody").find_all("tr", recursive=False)
# for tr in rows[1:]:
#     cols = tr.find_all("td")
#     week = int(cols[5].text)
#     dag = day
#     print(30*'-')
#     print(day)
#     print("Course :" + cols[2].text + "\n")
#     print("Start Time :" + cols[0].text + "\n")
#     print("End Time :" + cols[1].text + "\n")
#     print("Teacher :" + cols[3].text + "\n")
#     print("Location :" + cols[4].text + "\n")
#     print("Week :" + cols[5].text + "\n")
#     print("Date :", get_date(week, dag), "\n")
#     print(30*'-')




# driver = webdriver.PhantomJS()
#
# driver.get('http://keaplan.kea.dk/sws/prodF2018/default.aspx')
#
# main_window = None
# popup_window = None
# while main_window is None:
#         main_window = driver.current_window_handle
#
# driver.find_element_by_id('LinkBtn_studentset').click()
# filter_select = Select(driver.find_element_by_id('dlFilter'))  # select BYG
# filter_select.select_by_visible_text(department)
# obj_select = Select(driver.find_element_by_id('dlObject'))  # select BAKINT-2I
# options = obj_select.options
# for opt in options:
#     print(opt.text)

# obj_select.select_by_visible_text(team)
# week_select = Select(driver.find_element_by_id('lbWeeks'))
# week_select.select_by_visible_text('Week 05')

# driver.find_element_by_id('bGetTimetable').click()
#
# while popup_window is None:  # get handle to popup
#     for handle in driver.window_handles:
#         if handle == main_window:
#             continue
#         popup_window = handle
#         break
#
# driver.switch_to.window(popup_window)
# file_ = open('nobrowser.html', 'w')
# file_.write(driver.page_source)
# file_.close()
#
# soup = BeautifulSoup(open("nobrowser.html"), 'lxml')
# pretty = soup.prettify("utf-8")
#
# table = soup.find("table",{ "class": "grid-border-args"})
# rows = table.find("tbody").find_all("tr", recursive=False)
# print(rows)
#
# for tr in rows:




## parsing schedule array to events ##
# events = []
# for course in foo[1]:
#     col_no = 0
#     if isinstance(course, tuple):
#         event_start = foo[0][col_no]
#         event_end = foo[0][col_no+course[1]]
#         event = (course[0],event_start,event_end)
#         events.append(event)
#     col_no = col_no + 1
